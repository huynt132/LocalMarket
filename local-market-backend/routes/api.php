<?php

use App\Http\Controllers\Api\BuyerController;
use App\Http\Controllers\Api\ProductController;
// use Api\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ShopController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ScheduleController;
use App\Http\Controllers\Api\RevenueController;


Route::group(["middleware" => "auth:api"], function () {
    Route::get('/me', 'UserController@me')->name('api.me');
    Route::post('/change-details', 'UserController@changeDetails')->name('api.change-details');
    Route::post('/upload', 'UserController@uploadImage')->name('api.upload');

    //crud shop
    Route::get('/shops/{id}', [ShopController::class, 'show']);
    Route::post('/shops', [ShopController::class, 'store']);
    Route::get('/shops', [ShopController::class, 'index']);
    Route::post('/shops/{id}', [ShopController::class, 'update']);
    // Route::delete('/shops/{id}', [ShopController::class, 'destroy']);

    //crud products
    Route::post('/products', [ProductController::class, 'store']);
    Route::post('/products/{id}', [ProductController::class, 'update']);
    Route::delete('/products/{id}', [ProductController::class, 'destroy']);
    Route::get('/shop/products', [ProductController::class, 'getProductShop']);
    Route::get('shop/manage-products', [ShopController::class, 'getProductStatusShop']); //list all

    //crud schedule
    Route::get('/schedules', [ScheduleController::class, 'index']);
    Route::get('/schedules/{id}', [ScheduleController::class, 'show']);
    Route::post('/schedules', [ScheduleController::class, 'store']);
    Route::put('/schedules/{id}', [ScheduleController::class, 'update']);
    Route::delete('/schedules/{id}', [ScheduleController::class, 'destroy']);


    Route::post('/productsToSchedule', [ScheduleController::class, 'addProductToSchedule']);

    Route::post('/addToCart', [BuyerController::class, 'addToCart']);
    Route::get('user/cart-items', [BuyerController::class, 'getListCartItem']);
    Route::post('/removeItem', [BuyerController::class, 'removeFromCart']);
    Route::post('/purchase', [BuyerController::class, 'purchaseProcessing']);
    Route::get('/merchant/getOrderItems', [OrderController::class, 'getOrderDetailByStatus']);

    Route::get('activeProduct', [ProductController::class, 'getActiveProduct']); // list active

    Route::get('hiddenProduct', [ProductController::class, 'getHideProduct']);

    Route::post('order/changeStatusOrder', [OrderController::class, 'changeStatusOrder']);

    Route::get('shop/order-items', [OrderController::class, 'listOrderItemShop']);

    Route::get('/shop/outOfStockProducts', [ProductController::class, 'outOfStockProducts']); // san pham het hang

    Route::get('user/revenue', [RevenueController::class, 'getListRevenue']);

    Route::get('/user/getOrderList', [BuyerController::class, 'getOrderList']); //list order

    Route::get('/revenueByName', [RevenueController::class, 'getRevenueByName']);

    Route::get('/revenueByTime', [RevenueController::class, 'getRevenueByTime']);
    // Route::get('/search',[ProductController::class,'abc']);
    // 
    Route::post('/advancedSearchProduct', [ProductController::class, 'advancedSearchProduct']);

    Route::get('/shop/schedule/addToScheduleList', [ProductController::class, 'advancedSearchProduct']);
});

Route::get('/shop/productdetail/{id}', [ProductController::class, 'show']);
Route::get('/api/login/{provider}/redirect', 'SocialiteAuthController@redirectToProvider')->name('api.social.redirect');
Route::get('/login/{provider}/callback', 'SocialiteAuthController@handleProviderCallback')->name('api.social.callback');

Route::post('/register', 'Auth\RegisterController@register');
Route::post('/login', 'Auth\LoginController@login');

Route::get('/scheduleActives', [ScheduleController::class, 'listCurrentSchedules']);

Route::get('categories', [CategoryController::class, 'getListCategory']);

Route::get('units', [ProductController::class, 'getListUnit']);

Route::get('/productByCateId/{category_id}', [ProductController::class, 'listProductByCategory']);

Route::get('/searchProducts', [ProductController::class, 'searchProductByName']);

Route::get('/productOfSchedule/{schedule_id}', [ScheduleController::class, 'getProductBySchedule']); //(chƯa sỬa index (list sẢn phẨm thuỘc schedule cÙng true false))

Route::get('/getActiveProductOnSchedule/{schedule_id}', [ScheduleController::class, 'getActiveProductOnSchedule']);

Route::get('/shop/products/{id}', [ProductController::class, 'show']);

// Route::get('/products', [ProductController::class, 'index']);  //list all product on active all schedules
