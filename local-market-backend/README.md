# The Local Market Project


## How to start development?

1. Pull project

2. Create env file

```bash
cp .env.example .env
```

3. Start Docker containers

```bash
./vendor/bin/sail up
```

*** Docker for Windows and WSL2 will be required.

Go to http:localhost to check project is running.


