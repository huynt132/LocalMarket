<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->foreignId('shop_id')->references('id')->on('shops')->onDelete('cascade')->onUpdate('cascade'); // on delete cascade = xoa shopid -> tat ca schedules cung bi xoa
            $table->text('stall_description');
            $table->dateTime('active_from');
            $table->dateTime('active_to');
            $table->dateTime('delivery_deadline_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
