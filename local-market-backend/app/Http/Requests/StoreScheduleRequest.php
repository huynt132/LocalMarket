<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stall_description' => 'required|max:1000',
            'active_from' => 'required|date_format:Y-m-d H:i:s',
            'active_to' => 'required|date_format:Y-m-d H:i:s',
            'delivery_deadline_time' => 'required|date_format:Y-m-d H:i:s',
            'shop_id' => 'required'
        ];
    }
}
