<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Warehouse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{


    /**
     * Change status of order items in "order management page"
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatusOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_item_id' => 'required|integer|exists:order_items,id',
            'status' => 'required|integer' //1 = pending  2 = confirmed (shipping), 3 = completed, 4 = canceled
        ]);

        $shop = User::findOrFail(Auth::id())->shop;
        if (empty($shop)) {
            return response()->error('shop not found', 404);
        }

        if ($validator->fails()) {
            return response()->error($validator->errors(), 400);
        }

        $order_item = OrderItem::find($request->order_item_id);

        if (empty($order_item)) {
            return response()->error('order item not found', 200);
        }

        if ($order_item->product->shop->id != $shop->id) {
            return response()->error('permission denied', 404);
        }

        if (($order_item->status == 1 && $request->status != 2 && $request->status != 4)
            || ($order_item->status == 2 && $request->status != 3 && $request->status != 4)
            || $order_item->status == 3
            || $order_item->status == 4
        ) {
            return response()->error('invalid status', 400);
        }

        if ($order_item->status == 2 && $request->status == 3) {
            $warehouse = Warehouse::where('product_id', '=', $order_item->product_id)->first();

            $newQuantity =  $warehouse->quantity - $order_item->quantity;

            $result = DB::table('warehouses')
                ->where('id', '=', $warehouse->id)
                ->update([
                    'quantity' => $newQuantity,
                ]);
        }
        $order_item->status = $request->status;
        if ($request->status == 3) {
            $order_item->completion_time = Carbon::now()->toDateTimeString();
        }
        $order_item->save();
        return response()->success('changed' . $order_item . 'status = ' . $order_item->status, 200);
    }

    /**
     *  get all order items with information of product and purchaser in "oder management page"
     *
     * @return \Illuminate\Http\Response
     */
    public function getOrderDetailByStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'searchName' => 'string|min:1|max:255',
            //status: 1: pending, 2:confirm, 3:success, 4:canceled
            'status' => 'required|integer|min:0|max:4'
        ]);
        if ($validator->fails()) {
            return response()->error($validator->errors(), 400);
        }

        $shop_id = Shop::where('user_id', '=', Auth::id())->first()->id;

        //get all order items with information of product and purchaser
        $order_items = Product::select([
            'products.id', 'products.name', 'products.description',
            'shop_id', 'cost_per_unit', 'products.unit_id',
            'users.id as user_id', 'users.name as user_name', 'address', 'phone_number', 'products.created_at', 'quantity', 'order_items.id as order_item_id'
        ])
            ->join('order_items', 'products.id', '=', 'order_items.product_id')
            ->join('orders', 'order_items.order_id', '=', 'orders.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->where('products.shop_id', '=', $shop_id)
            ->where('order_items.status', '=', $request->status)
            ->with([
                'categories:id,name', 'warehouse:id,product_id,quantity', 'unit:id,title',
                'images' => function ($query) {
                    $query->where('type', 1)->select('id', 'product_id', 'images.content', 'type');
                }
            ]);

        if (isset($request->searchName)) {
            $order_items->where('users.name', 'like', '%' . $request->searchName . '%');
        };
        return response()->success($order_items->paginate(12), 200);
    }

    /**
     * Display order items for merchant in "order management page"
     *
     * @return \Illuminate\Http\Response
     */
    // list all order of shop
    public function listOrderItemShop()
    {
        $shop = User::findOrFail(Auth::id())->shop;

        if (empty($shop)) {
            return response()->error('shop not found', 404);
        }
        $order_items = OrderItem::join('products', 'order_items.product_id', '=', 'products.id')
            ->where('products.shop_id', $shop->id)
            ->join('orders', 'order_items.order_id', '=', 'orders.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->select(
                'order_items.id',
                'order_items.order_id',
                'products.shop_id',
                'order_items.product_id',
                'products.name as product_name',
                'products.cost_per_unit',
                'order_items.quantity',
                DB::raw('order_items.quantity * products.cost_per_unit as price'),
                'order_items.created_at',
                'order_items.updated_at',
                'users.id as user_id',
                'users.name as user_name',
                'users.address as user_address',
                'users.phone_number as user_phone_number',
                'order_items.status',
            )
            ->with([
                'product:id',
                'product.images' => function ($query) {
                    $query->where('type', 1)->select('id', 'product_id', 'content', 'type');
                }
            ])
            ->get();

        // 1 = pending, 2 = confirmed (shipping), 3 = completed, 4 = canceled
        $count_pending = $order_items->where('status', 1)->count();
        $count_confirmed = $order_items->where('status', 2)->count();
        $count_completed = $order_items->where('status', 3)->count();
        $count_canceled = $order_items->where('status', 4)->count();
        
        return response()->success([
            'order_items' => $order_items,
            'count_order_pending' => $count_pending,
            'count_order_confirmed' => $count_confirmed,
            'count_order_completed' => $count_completed,
            'count_order_canceled' => $count_canceled
        ], 200);
    }
}
