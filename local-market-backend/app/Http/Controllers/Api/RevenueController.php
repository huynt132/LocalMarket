<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RevenueController extends Controller
{
    /**
     * Get list successfully delivered products and net revenue between start_time and end_time.
     *
     * @return \Illuminate\Http\Resquest $request
     * @return \Illuminate\Http\Response
     */
    public function getRevenueByTime(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_time' => 'required|date_format:Y-m-d H:i:s',
            'end_time' => 'required|date_format:Y-m-d H:i:s'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => "Validation error!",
                'errors' => $validator->errors()
            ], 400);
        }

        $shop = $this->getShop();
        if (isset($shop->fail)) {
            return $shop->error;
        }

        $product_ids = $this->getProductIDs($shop);
        if (isset($product_ids->fail)) {
            return $product_ids->error;
        }

        $sold_products = $this->getProductByTime($product_ids, $request->start_time, $request->end_time);
        if (isset($sold_products->fail)) {
            return $sold_products->error;
        }

        $products = $this->convertToBill($sold_products);

        return response()->json([
            'success' => true,
            'data' => [
                'total_amount' => $products->sum('total'),
                'products' => $products
            ]
        ], 200);
    }

    /**
     * Get list successfully delivered products and net revenue by product name.
     *
     * @return \Illuminate\Http\Resquest $request
     * @return \Illuminate\Http\Response
     */
    public function getRevenueByName(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'product_name' => 'required|max:150'
        ]);
      
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => "Validation error!",
                'errors' => $validator->errors()
            ], 400);
        }

        
        $shop = $this->getShop();
        if (isset($shop->fail)) {
            return $shop->error;
        }
        // return $request->product_name;
        $product_ids = $this->getProductIDs($shop);
        if (isset($product_ids->fail)) {
            return $product_ids->error;
        }

        $sold_products = $this->getProductByName($product_ids, $request->product_name);
        if (isset($sold_products->fail)) {
            return $sold_products->error;
        }

        $products = $this->convertToBill($sold_products);

        return response()->json([
            'success' => true,
            'data' => [
                'total_amount' => $products->sum('total'),
                'products' => $products
            ]
        ], 200);
    }

    private function getShop()
    {
        $shop = User::findOrFail(Auth::id())->shop;

        if (is_null($shop)) {
            return ((object) [
                'fail' => true,
                'error' => response()->json([
                    'success' => false,
                    'message' => 'CAN NOT FIND SHOP!'
                ], 404)
            ]);
        }

        return $shop;
    }

    private function getProductIDs($shop)
    {
        $products = $shop->products;

        if ($products->isEmpty()) {
            return ((object) [
                'fail' => true,
                'error' => response()->json([
                    'success' => false,
                    'message' => 'CAN NOT FIND ANY PRODUCTS!'
                ], 404)
            ]);
        }

        return $products->pluck('id');
    }

    private function getProductByTime($product_ids, $start_time, $end_time)
    {
        $sold_products = DB::table('order_items')
            ->join('products', 'products.id', 'order_items.product_id')
            ->leftJoin('images', 'products.id', 'images.product_id')
            ->whereIn('products.id', $product_ids->toArray())
            ->whereBetween('completion_time', [$start_time, $end_time])
            ->where('order_items.status', 3)
            ->where(function ($query) {
                $query->where('images.type', 1)
                      ->orWhereNull('images.type');
            })
            ->select('order_items.id','order_items.product_id', 'products.name', 'images.content', 'cost_per_unit', 'order_items.quantity')
            ->get()
            ->groupBy('product_id');

        // if ($sold_products->isEmpty()) {
        //     return ((object) [
        //         'fail' => true,
        //         'error' => response()->json([
        //             'success' => false,
        //             'message' => 'CAN NOT FIND ANY PRODUCTS WERE SOLD IN THIS PERIOD!'
        //         ], 404)
        //     ]);
        // }

        return $sold_products;
    }

    private function getProductByName($product_ids, $product_name)
    {
        $sold_products = DB::table('order_items')
            ->join('products', 'products.id', 'order_items.product_id')
            ->leftJoin('images', 'products.id', 'images.product_id')
            ->whereIn('products.id', $product_ids->toArray())
            ->where('products.name', 'like', '%' . $product_name . '%')
            ->where('order_items.status', 3)
            ->where(function ($query) use ($product_ids) {
                $query->where('images.type', 1)
                      ->orWhereNull('images.type');
            })
            ->select('order_items.id', 'order_items.product_id', 'products.name', 'images.content', 'cost_per_unit', 'order_items.quantity')
            ->get()
            ->groupBy('product_id');

        if ($sold_products->isEmpty()) {
            return ((object) [
                'fail' => true,
                'error' => response()->json([
                    'success' => false,
                    'message' => 'CAN NOT FIND ANY PRODUCTS WITH THIS NAME!'
                ], 404)
            ]);
        }

        return $sold_products;
    }

    private function convertToBill($sold_products)
    {
        $products = collect();

        $sold_products->each(function ($item, $key) use($products) {
            $products->push((object) [
                'p_id' => $item->first()->product_id,
                'p_name' => $item->first()->name,
                'p_image_path' => $item->first()->content,
                'p_unit_cost' => $item->first()->cost_per_unit,
                'total_quantities' => $item->sum('quantity'),
                'total' => ($item->first()->cost_per_unit * $item->sum('quantity'))
            ]);
        });

        return $products;
    }

    // public function getListRevenue(){
    //     $user_id=Auth::id();
    //     $order_items = OrderItem::select(
    //         'order_items.id',
    //         'order_items.product_id',
    //         'products.name as product_name',
    //         'products.cost_per_unit',
    //         'order_items.quantity',
    //         'units.title',
    //         DB::raw('products.cost_per_unit*order_items.quantity as total_payment'),
    //         'shops.name as shop_name',
    //         'images.content'
    //     )
    //     ->join('orders','orders.id','=','order_items.order_id')
    //     ->join('products','products.id','=','order_items.product_id')
    //     ->join('units','products.unit_id','=','units.id')
    //     ->join('shops','shops.id','=','products.shop_id')
    //     ->leftJoin(DB::raw('(select distinct images.id as image_id, images.content, images.type,
    //     images.product_id from images where type = 1) as images'), 'products.id', '=', 'images.product_id')
    //     ->where('orders.user_id','=',$user_id)
    //     ->where('order_items.status','=','3')
    //     ->get();
    //     return $order_items;
    // }
    

}
