<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Image;
use App\Models\Product;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use App\Models\CategoryProduct;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use  App\Http\Controllers\Api\ImageController;
use App\Models\Shop;
use App\Models\Unit;
use Exception;

class ProductController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shop = User::findOrFail(Auth::id())->shop;

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'description' => 'required|min:10|max:2000',
            'cost_per_unit' => 'required|numeric',
            'unit_id' => 'required|exists:units,id',
            'image_main' => 'required|mimes:jpg,jpeg,png',
            'images.*' => '',
            'categories.*' => 'required|exists:categories,id|numeric',
            'quantity' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->error($validator->errors(), 400);
        }

        DB::beginTransaction();
        try {
            //this user haven't create shop yet
            $product = Product::create([
                'shop_id' => $shop->id,
                'name' => $request->name,
                'description' => $request->description,
                'cost_per_unit' => $request->cost_per_unit,
                'unit_id' => $request->unit_id
            ]);

            $product->save();

            // table images
            $image_main = $request->file('image_main');

            if (!empty($image_main)) {
                ImageController::storeImage($image_main, null, $product->id, 1);
            }

            if ($request->hasFile('images')) {
                $images = $request->file('images');

                foreach ($images as $img) {
                    ImageController::storeImage($img, null, $product->id, 0);
                }
            }

            //table category_product
            foreach ($request->categories as $cate_id) {
                $category_product = CategoryProduct::create([
                    'category_id' => $cate_id,
                    'product_id' => $product->id
                ]);
                $category_product->save();
            }

            // table warehouse
            $warehouse = Warehouse::create([
                'product_id' => $product->id,
                'quantity' => $request->quantity
            ]);

            $warehouse->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error($e->getMessage(), 400);
        }

        return response()->success($product, 201);
    }

    /**
     * Show product information in "product detail page".
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('id', $id)
            ->select(
                'products.id',
                'products.name',
                'products.description',
                'products.cost_per_unit',
                'products.unit_id',
                'products.shop_id'
            )
            ->with([
                'categories:id,name', 'warehouse:id,product_id,quantity', 'unit:id,title',
                'images' => function ($query) {
                    $query->where('type', 1)->select('id', 'product_id', 'images.content', 'type');
                }
            ])->first();

        if (empty($product)) {
            return response()->json(['message' => 'product not found'], 404);
        }
        $shop = Shop::where('id', $product->shop_id)
            ->with(['image'])
            ->first();

        return response()->json([
            'success' => true,
            'data' => $product,
            'shop' => $shop
        ], 200);
    }

    /**
     * Update the specified product information in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:255',
            'description' => 'min:10|max:2000',
            'cost_per_unit' => 'numeric',
            'unit_id' => 'exists:units,id',
            'images_delete.*' => 'exists:images,id',
            'categories_add.*' => 'exists:categories,id',
            'categories_delete.*' => 'exists:categories_products,category_id',
            'quantity' => 'numeric'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $product = Product::with(['images', 'categories', 'unit'])->findOrFail($id);

        if (empty($product)) {
            return response()->json([
                'success' => false,
                'message' => 'product not found'
            ], 404);
        }

        DB::beginTransaction();
        try {
            // update product
            $product->update($request->all());

            // update images
            if (isset($request->images_delete)) {
                //delete all images request
                Image::whereIn('id', $request->images_delete)
                    ->where('product_id', $id)
                    ->where('type', '<>', 1)->delete();
            }

            if (!empty($request->file('image_main'))) {
                ImageController::updateImage($request->file('image_main'), null, $product->id, 1);
            }

            if (!empty($request->hasFile('images_add'))) {
                $images_add = $request->file('images_add');
                foreach ($images_add as $img) {
                    ImageController::storeImage($img, null, $product->id, 0);
                }
            }

            $product->warehouse->update($request->all());
            if (isset($request->categories_delete)) {
                CategoryProduct::where('product_id', $product->id)
                    ->whereIn('category_id', $request->categories_delete)
                    ->delete();
            }

            if (isset($request->categories_add)) {
                foreach ($request->categories_add as $cate_id) {
                    $category_product = CategoryProduct::create([
                        'category_id' => $cate_id,
                        'product_id' => $product->id
                    ]);
                    $category_product->save();
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error($e->getMessage(), 400);
        }
        return response()->success($product, 200);
    }

    /**
     * Remove the specified product from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shop = Shop::where('user_id', '=', Auth::id())->first();
        $product = Product::find($id);

        if (empty($product)) {
            return response()->error('product not found', 404);
        }

        if ($product->shop_id != $shop->id) {
            return response()->error('Access deny', 401);
        }
        $product->delete();

        return response()->success("Product " . $product->id . " deleted", 200);
    }

    /**
     * get list of unit items from storage
     *
     * @return \Illuminate\Http\Response
     */
    public function getListUnit()
    {
        $units = Unit::select('id', 'title')->get();
        return response()->success($units, 200);
    }

    public function advancedSearchProduct(Request $request)
    {
        //validate
        $validator = Validator::make($request->all(), [
            'keyword' => 'string|max:255',
            'productQuantityFrom' => 'integer|min:0',
            'productQuantityTo' => 'integer|min:0',
            'activeProduct' => 'integer|min:0|max:2',
            'category' => 'integer|min:1|exists:categories,id',
            'authCheck' => 'integer|min:0|max:1',
            'schedule' => 'integer|min:1|exists:schedules,id',
            // 'limit' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            return response()->error($validator->errors(), 400);
        }

        $search_option_hit = false;
        $products = Product::query();

        //search required merchant role(owned product)
        if ($request->authCheck == 1) {
            $shop_id = Shop::where('user_id', '=', Auth::id())->first()->id;
            $products->where('products.shop_id', '=', $shop_id);
        }

        //search by product quantity
        if (isset($request->productQuantityFrom)) {
            $products = $products->join('warehouses', 'warehouses.product_id', '=', 'products.id')
                ->whereBetween('warehouses.quantity', [$request->productQuantityFrom, $request->productQuantityTo]);

            $search_option_hit = true;
        }

        //trạng thái hoạt động
        if (isset($request->activeProduct) || isset($request->schedule)) {
            $products = $products->leftJoin('products_schedules', 'products.id', '=', 'products_schedules.product_id')
                ->distinct();

            if (isset($request->activeProduct)) {
                $products = $products->leftJoin('schedules', 'products_schedules.schedule_id', '=', 'schedules.id');

                //đã ẩn
                if ($request->activeProduct == 0) {
                    $products = $products->where('active_from', '>', 'now()')
                        ->orWhere('active_to', '<', 'now()');
                }

                //đang hoạt động
                if ($request->activeProduct == 1) {
                    $products = $products->where([['active_from', '<=', 'now()'], ['active_to', '>', 'now()']]);
                }

                //tất cả các sản phẩm cùng với trạng thái đang hoạt động và đã ẩn
                if ($request->activeProduct == 2) {
                    $products = $products->addSelect(DB::raw('(CASE WHEN active_from <= now()
                AND active_to > now() THEN true ELSE false END) AS active_p'));
                }

                //lấy ra tổng số lượng đã bán
                $products = $products->with([
                    'orderItems' => function ($query) {
                        $query->where('status', 1)
                            ->groupBy('product_id')
                            ->select(
                                'product_id',
                                DB::raw('sum(quantity) as quantity_sold')
                            );
                    }
                ]);
            }
            //sản phẩm đang không active trên lịch
            if (isset($request->schedule)) {
                $products = $products
                    ->where(function ($query) use ($request) {
                        $query->where('products_schedules.schedule_id', '<>', $request->schedule)
                            ->orWhereNull('products_schedules.schedule_id');
                    });
            }

            $search_option_hit = true;
        }

        //product by category
        if (isset($request->category)) {
            $products = $products->join('categories_products', 'products.id', '=', 'categories_products.product_id')
                ->where('categories_products.category_id', '=', $request->category);

            $search_option_hit = true;
        }

        //product by name
        if (isset($request->keyword)) {
            $products = $products->where('products.name', 'like', '%' . $request->keyword . '%');

            $search_option_hit = true;
        }

        if (!$search_option_hit) {
            return response()->error("invalid search option", 400);
        }

        $products->addSelect(
            'products.id',
            'products.name as product_name',
            'products.description',
            'products.cost_per_unit',
            'products.unit_id',
            'products.shop_id',
        );

        $products->with([
            'categories:id,name', 'warehouse:id,product_id,quantity', 'unit:id,title',
            'images' => function ($query) {
                $query->where('type', 1)->select('id', 'product_id', 'images.content', 'type');
            }
        ]);

        return response()->success($products->get(), 200);
    }
}
