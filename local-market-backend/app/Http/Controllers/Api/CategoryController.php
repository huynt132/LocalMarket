<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function getListCategory()
    {
        $categories = DB::table('categories')->get();
        if (empty($categories)) {
            return response()->error('Category not found!', 404);
        }
        return response()->success($categories, 200);
    }
}
