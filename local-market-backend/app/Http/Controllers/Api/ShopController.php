<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Api\ImageController;
use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use Exception;

class ShopController extends Controller
{
    /**
     * Display a listing of the shop information.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shop = Shop::where('user_id', '=', Auth::id())->with(['image' => function ($query) {
            $query->where('type', 1)->select('id', 'shop_id', 'images.content', 'type');
        }])->first();

        if (empty($shop)) {
            return response()->error('shop not found', 404);
        }
        return response()->success($shop, 200);
    }

    /**
     * Store a newly created shop in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'shop_name' => 'invalid input of shop name, string and length in [2,255]',
            'shop_description' => 'invalid input of shop description, string and length in [10,2000]',
            'image' => "shop's image is required in jpg,jpeg,png,csv,txt,xlx,xls,pdf file"
        ];

        $validator = Validator::make($request->all(), [
            'shop_name' => 'required|string|max:255|min:2',
            'shop_description' => 'required|string|min:10|max:2000',
            'image' => 'required||mimes:jpg,jpeg,png,csv,txt,xlx,xls,pdf'
        ], $messages);

        if ($validator->fails()) {
            return response()->error($validator->errors(), 400);
        }

        $user_id = Auth::id();

        //check if that user already have a shop or not
        $user = User::find($user_id);
        if (!is_null($user->shop)) {
            return response()->error('This user already owned a shop', 403);
        };

        DB::beginTransaction();
        try {
            //this user haven't create shop yet
            $shop = new Shop();
            $shop->name = $request->shop_name;
            $shop->description = $request->shop_description;
            $shop->user_id = $user_id;
            $shop->save();

            ImageController::storeImage($request->file('image'), $shop->id, null, 1);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error($e->getMessage(), 400);
        }

        return response()->success($shop, 201);
    }

    /**
     * Display the specified shop by id.
     *
     * @param  int  $id 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //validate
        if (!is_numeric($id) || $id <= 0) {
            return response()->error('The given data was invalid.', 422);
        }

        try {
            $shop = Shop::findOrFail($id)->with('image')->get();
        } catch (Exception $e) {
            return response()->error($e->getMessage(), 404);
        }
        return response()->success($shop, 200);
    }

    /**
     * Update the specified shop in storage by id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id - shop id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'name' => 'name must be string and length between [2,255]',
            'description' => 'name must be string and length between [10,2000]',
            // 'image' => "image must in type of jpg,jpeg,png,csv,txt,xlx,xls,pdf"
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'string|min:2|max:255',
            'description' => 'string|min:10|max:2000',
            // 'image' => 'mimes:jpg,jpeg,png,csv,txt,xlx,xls,pdf'
        ], $messages);

        if ($validator->fails()) {
            return response()->error($validator->errors(), 400);
        }
        //check permission
        try {
            $shop = User::findOrFail(Auth::id())->shop;
        } catch (Exception $e) {
            return response()->error($e->getMessage(), 404);
        }
        if ($shop->id != intval($id)) {
            return response()->error('Access deny', 401);
        }

        DB::beginTransaction();
        try {
            $shop->update($request->all());
            $new_image = $request->file('image');

            if (!empty($new_image)) {
                //add image
                ImageController::updateImage($new_image, $shop->id, null, 1);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error($e->getMessage(), 400);
        }
        return response()->success($shop, 200);
    }

    /**
     * Remove the specified shop from storage by id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //validate
        if (!is_numeric($id) || $id <= 0) {
            return response()->error('The given data was invalid.', 422);
        }

        $shop = Shop::findOrFail($id);
        $shop->delete();
        return response()->success('delete success', 204);
    }

    /**
     * get active product by status from storage for "product management page" .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProductStatusShop()
    {
        //create time, image, product name, categori, quantity, status, tong tien, username, address, phone number
        $shop = User::find(Auth::id())->shop;
        if (empty($shop)) {
            return response()->error('shop not found', 404);
        }

        $products = Product::where('products.shop_id', $shop->id)
            ->leftJoin('products_schedules', 'products.id', '=', 'products_schedules.product_id')
            ->leftJoin('schedules', 'products_schedules.schedule_id', '=', 'schedules.id')
            ->select(
                'products.id as id',
                'products.shop_id',
                'products.name',
                'products.cost_per_unit',
                'products.unit_id',
                'schedules.id as schedule_id',
                'schedules.active_from',
                'schedules.active_to',
                DB::raw('(CASE WHEN active_from <= NOW() AND active_to >= NOW() THEN true ELSE false END) AS active_p')
            )
            ->with([
                'images' => function ($query) {
                    $query->where('type', 1)->select(
                        'id',
                        'product_id',
                        'images.content',
                        'type'
                    );
                },
                'categories:id,name',
                'warehouse:id,product_id,quantity',
                'unit:id,title',
                'orderItems' => function ($query) {
                    $query->where('status', 1)
                        ->groupBy('product_id')
                        ->select(
                            'product_id',
                            DB::raw('sum(quantity) as quantity_sold')
                        );
                }
            ])
            ->paginate(12);

        return response()->success($products, 200);
    }
}
