<?php

namespace App\Http\Controllers\Api;

use App\Models\Image;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class ImageController extends Controller
{
    public static function storeImage($upload_image, $shop_id, $product_id, $type)
    {
        //gen unique image name
        $image_file_path = date('YmdHis');

        $imageFileType = strtolower(pathinfo($upload_image, PATHINFO_EXTENSION));

        // Convert to base64 
        $image_base64 = base64_encode(file_get_contents($upload_image));
        $image_content = 'data:image/' . $imageFileType . ';base64,' . $image_base64;

        // Insert record
        $image = new Image();
        $image->name = $image_file_path;
        $image->content = $image_content;
        $image->shop_id = $shop_id;
        $image->product_id = $product_id;
        $image->type = $type;
        $image->save();

        return $image;
    }

    /*
    * This method will search image by product id or shop id than update file of founded record
    *
    */
    public static function updateImage($update_image, $shop_id, $product_id, $type)
    {
        //gen unique image name
        $imageFileType = strtolower(pathinfo($update_image, PATHINFO_EXTENSION));

        // Convert to base64 
        $image_base64 = base64_encode(file_get_contents($update_image));
        $image_content = 'data:image/' . $imageFileType . ';base64,' . $image_base64;

        // Insert record
        $current_image = Image::query();
     
        if (!is_null($shop_id)) {
            $current_image = $current_image->where('shop_id', '=', $shop_id)->where('type', '=', $type);
        }

        if (!is_null($product_id)) {
            $current_image = $current_image->where('product_id', '=', $product_id)->where('type', '=', $type);
        }

        $current_image->update([
            'content' => $image_content,
            'shop_id' => $shop_id,
            'product_id' => $product_id,
            'type' => $type,
            'updated_at' =>  Carbon::now()->toDateTimeString(),
        ]);

        return $current_image->first();
    }
}
