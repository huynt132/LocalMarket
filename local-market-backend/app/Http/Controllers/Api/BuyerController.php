<?php

namespace App\Http\Controllers\Api;

use App\Events\SendNotification;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use App\Models\Warehouse;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use stdClass;

class BuyerController extends Controller
{
    /**
     * Display order item that buyer placed order
     *
     * @return \Illuminate\Http\Response
     */
    public function getOrderList()
    {
        $user_id = Auth::id();

        $order_items = OrderItem::select(
            'order_items.id',
            'order_items.product_id',
            'products.name as product_name',
            'products.cost_per_unit',
            'order_items.quantity',
            'units.title',
            DB::raw('(products.cost_per_unit * order_items.quantity) as total_payments'),
            'order_items.status',
            'order_items.completion_time',
            'shops.name as shop_name',
            'images.content'
        )
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('units', 'products.unit_id', '=', 'units.id')
            ->join('shops', 'shops.id', '=', 'products.shop_id')
            ->leftJoin(DB::raw('(select distinct images.id as image_id, images.content, images.type,
             images.product_id from images where type = 1) as images'), 'products.id', '=', 'images.product_id')
            ->where('orders.user_id', '=', $user_id)
            ->where('order_items.status', '=', '3')->paginate(12);
        $total_items = OrderItem::select(
            DB::raw('(products.cost_per_unit * order_items.quantity) as total_payments'),
        )
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->where('orders.user_id', '=', $user_id)
            ->where('order_items.status', '=', '3')->get();
        return response()->json([
            'success' => true,
            'data' => $order_items,
            'total' => $total_items
        ], 200);
    }

    /**
     * processing for a purchase from cart to order item
     *
     * @return \Illuminate\Http\Response
     */
    public function purchaseProcessing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'purchase_products' => 'required|array',
            'purchase_products.*.product_id' => 'required|integer|min:1|exists:products,id',
            'purchase_products.*.quantity' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" =>   $validator->errors()
            ], 400);
        }

        $purchase_products = $request->purchase_products;

        $buyer = User::find(Auth::id());

        $order = new Order();
        $order->user_id = $buyer->id;
        $order->save();

        $message_infors = array();

        //avoid using row by row update, instead we use batch update to protect the integrity of data
        DB::beginTransaction();
        try {
            foreach ($purchase_products as $p) {
                $order_item = new OrderItem();
                $order_item->product_id = $p['product_id'];
                $order_item->quantity = $p['quantity'];
                $order_item->status = 1; //1 = pending
                $order_item->order_id = $order->id;

                $order_item->save();

                //send noti
                $noti_infor = User::join('shops', 'shops.user_id', '=', 'users.id')
                ->join('products', 'products.shop_id', '=', 'shops.id')
                ->leftJoin('units', 'units.id', '=', 'products.unit_id')
                ->where('products.id', $p['product_id'])
                ->select('users.id', 'units.title', 'products.name')
                ->first();

                $noti_temp = new stdClass();
                $noti_temp->to_id = $noti_infor->id;
                $noti_temp->from_name = $buyer->name;
                $noti_temp->time = Carbon::now()->toDateTimeString();
                $noti_temp->message = "Vừa mua " . $order_item->quantity . " " . $noti_infor->title . " " . $noti_infor->name;
                broadcast(new SendNotification($noti_temp));

                array_push($message_infors, $noti_temp);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success" => false,
                "message" => $e->getMessage()
            ], 400);
        }
        //delete current shopping cart
        DB::table('carts')->where('user_id', '=',  $buyer->id)->delete();

        return response()->json([
            "success" => true,
        ], 200);
    }

    /**
     * add product to buyer's shopping cart
     *
     * @return \Illuminate\Http\Response
     */
    public function addToCart(Request $request)
    {
        //validate
        $validator = validator::make($request->all(), [
            'product_id' => 'required|integer|min:1',
            'quantity' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" =>   $validator->errors()
            ], 400);
        }

        $quantity = $request->quantity;
        $product_id = $request->product_id;

        $cart = Cart::where('user_id', '=', Auth::id())->firstOrCreate(
            ['user_id' =>  Auth::id()],
        );

        //check if product is avaiable by time
        $product_in_db = DB::table('products_schedules')
            ->select('product_id')
            ->join('schedules', 'schedules.id', '=', 'schedule_id')
            ->where('product_id', '=', $product_id)
            ->where([
                ['active_from', '<', 'now()'],
                ['active_to', '>', 'now()'],
            ])
            ->first();

        if (empty($product_in_db)) {
            return response()->json([
                "success" => false,
                "message" => "This product is no longer available"
            ], 400);
        }

        //check valid quantity in stock
        $stock = Warehouse::where('product_id', '=', $product_id)
            ->first();

        if ($quantity > $stock->quantity) {
            return response()->json([
                "success" => false,
                "message" => "Not enough quantity in stock"
            ], 400);
        }

        $cart_item = new CartItem();
        $cart_item->cart_id = $cart->id;
        $cart_item->product_id = $product_id;
        $cart_item->quantity = $quantity;
        $cart_item->save();

        return response()->json([
            "success" => true,
            "data" =>   $cart_item
        ], 201);
    }

    /**
     * delete cart item from shopping cart
     *
     * @return \Illuminate\Http\Response
     */
    public function removeFromCart(Request $request)
    {
        // validate
        $validator = validator::make($request->all(), [
            'delete_cart_items' => 'required|array',
            'delete_cart_items.*' => 'required|integer|min:1|exists:cart_items,id',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "message" =>   $validator->errors()
            ], 400);
        }

        // check permission
        $user_id = Auth::id();
        $delete_list = $request->delete_cart_items;

        $owned_cart_items = CartItem::join('carts', 'carts.id', '=', 'cart_items.cart_id')
            ->join('users', 'users.id', '=', 'carts.user_id')
            ->where('users.id', '=', $user_id)
            ->whereIn('cart_items.id', $delete_list);

        if (sizeOf($delete_list) != $owned_cart_items->count()) {
            return response()->json([
                "success" => false,
                'message' => 'Access deny'
            ], 401);
        };

        $owned_cart_items->delete();

        return response()->json([
            "success" => true,
            "message" => 'delete success'
        ], 204);
    }

    /**
     * Display cart items from shoppingcart
     *
     * @return \Illuminate\Http\Response
     */
    public function getListCartItem()
    {

        $cart = Cart::where('user_id', '=', Auth::id())->firstOrCreate(
            ['user_id' =>  Auth::id()],
        );

        $cart_items = CartItem::where('cart_id', $cart->id);

        if (empty($cart_items)) {
            return response()->json([
                'success' => false,
                'message' => "cart not found"
            ], 404);
        }

        $cart_items = $cart_items
            ->join('products', 'cart_items.product_id', '=', 'products.id')
            ->with([
                'product:id,name,shop_id,unit_id',
                'product.warehouse' => function ($query) {
                    $query->select('id', 'product_id', 'quantity');
                },
                'product.images' => function ($query) {
                    $query->where('type', 1)->select('id', 'product_id', 'content');
                },
                'product.unit' => function ($query) {
                    $query->select('id', 'title');
                },

            ])
            ->select(
                'cart_items.id',
                'cart_items.cart_id',
                'products.id as product_id',
                'quantity',
                DB::raw('cart_items.quantity * products.cost_per_unit as price')
            )
            ->get();

        return response()->json([
            'success' => true,
            'data' => $cart_items
        ], 200);
    }
}
