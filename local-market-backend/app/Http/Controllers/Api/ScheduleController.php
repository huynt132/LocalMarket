<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductSchedule;
use App\Models\Schedule;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Exception;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $shop = $this->getShop();
        if (isset($shop->fail)) {
            return $shop->error;
        }

        $schedules = DB::table('schedules')
            ->where('shop_id', $shop->id)
            ->select('id', 'name', 'stall_description', 'active_from', 'active_to', 'delivery_deadline_time', 'shop_id')
            ->paginate(12);
        if ($schedules->isEmpty()) {
            return response()->error('CAN NOT FIND ANY SCHEDULES!', 404);
        }

        return response()->success($schedules, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:150',
            'stall_description' => 'required|max:1000',
            'active_from' => 'required',
            'active_to' => 'required',
            'delivery_deadline_time' => 'required',
        ]);

        $shop = $this->getShop();

        if (isset($shop->fail)) {
            return $shop->error;
        }

        $validator = $this->validateSchedule($request);
        if (isset($validator->fail)) {
            return $validator->error;
        }

        $schedule = Schedule::create([
            'name' => $request->name,
            'stall_description' => $request->stall_description,
            'active_from' => $request->active_from,
            'active_to' => $request->active_to,
            'delivery_deadline_time' => $request->delivery_deadline_time,
            'shop_id' => $shop->id
        ]);

        unset($schedule->created_at);
        unset($schedule->updated_at);

        return response()->success($schedule, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shop = $this->getShop();
        if (isset($shop->fail)) {
            return $shop->error;
        }

        $schedule = $shop->schedules->find($id);

        if (empty($schedule)) {
            return response()->error('ID CAN NOT BE FOUND!', 404);
        }

        unset($schedule->created_at);
        unset($schedule->updated_at);

        return response()->success($schedule, 202);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:150',
            'stall_description' => 'required|max:1000',
            'active_from' => 'required',
            'active_to' => 'required',
            'delivery_deadline_time' => 'required',
        ]);

        $shop = $this->getShop();

        if (isset($shop->fail)) {
            return $shop->error;
        }

        $validator = $this->validateSchedule($request);
        if (isset($validator->fail)) {
            return $validator->error;
        }

        $schedule = $shop->schedules->find($id);

        if (empty($schedule)) {
            return response()->error("ID CAN NOT BE FOUND!", 404);
        }

        $schedule->update([
            'name' => $request->name,
            'stall_description' => $request->stall_description,
            'active_from' => $request->active_from,
            'active_to' => $request->active_to,
            'delivery_deadline_time' => $request->delivery_deadline_time,
            'shop_id' => $shop->id
        ]);

        unset($schedule->created_at);
        unset($schedule->updated_at);

        return response()->success($schedule, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = Schedule::find($id);
        if ($schedule->id != $id) {
            return response()->error('Access deny', 401);
        }
        $shop = $this->getShop();
        if (isset($shop->fail)) {
            return $shop->error;
        }

        $schedule = $shop->schedules->find($id);

        if (is_null($schedule)) {
            return response()->error('ID CAN NOT BE FOUND!', 404);
        }

        Schedule::destroy($id);

        return response()->success($schedule, 200);
    }

    /**
     * Display a listing of the current schedules.
     *
     * @return \Illuminate\Http\Response
     */
    public function listCurrentSchedules(Request $request)
    {
        $current_date_time = Carbon::now()->toDateTimeString();

        $schedules = DB::table('schedules')
            ->where([
                ['active_from', '<', $current_date_time],
                ['active_to', '>', $current_date_time]
            ])
            ->join('shops', 'schedules.shop_id', '=', 'shops.id')
            ->leftJoin('images', 'images.shop_id', '=', 'schedules.shop_id')
            ->select('schedules.id', 'schedules.name', 'stall_description', 'delivery_deadline_time', 'images.content', 'shops.name as shop_name')
            ->paginate(12);

        if ($schedules->isEmpty()) {
            return response()->error('CAN NOT FIND ANY SCHEDULES!', 404);
        }
        return response()->success($schedules, 200);
    }

    private function getShop()
    {
        $shop = User::findOrFail(Auth::id())->shop;

        if (is_null($shop)) {
            return ((object) [
                'fail' => true,
                'error' => response()->json([
                    'success' => false,
                    'message' => 'CAN NOT FIND SHOP!'
                ], 404)
            ]);
        }

        return $shop;
    }

    private function validateSchedule($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:150',
            'stall_description' => 'required|max:1000',
            'active_from' => 'required',
            'active_to' => 'required',
            'delivery_deadline_time' => 'required',
        ]);

        if ($validator->fails()) {
            return ((object) [
                'fail' => true,
                'error' => response()->json([
                    'success' => false,
                    'message' => "Validation error!",
                    'errors' => $validator->errors()
                ], 400)
            ]);
        }
    }


    /**
     * add product from prodcut list to schedule by schedule id".
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addProductToSchedule(Request $request)
    {
        //validate
        $validator = Validator::make($request->all(), [
            'schedule_id' => 'required|integer|min:1',
            'products'   => 'required|array',
            'products.*' => 'integer|min:1',
        ]);

        if ($validator->fails()) {
            return response()->error($validator->errors(), 400);
        }

        $products = $request->products;
        $schedule_id = $request->schedule_id;

        //check permission
        $user_id = Auth::id();
        $schedule = User::findOrFail($user_id)
            ->shop
            ->schedules
            ->where('id', '=', $schedule_id)->first();

        //this user dont have permission to this schedule id
        if ($schedule->id != $schedule_id) {
            return response()->error('Access deny', 401);
        }

        $owned_products = User::findOrFail($user_id)
            ->shop
            ->products
            ->whereIn('id', $products);

        if (sizeOf($products) != $owned_products->count()) {
            return response()->error('Access deny', 401);
        };

        $result = array();
        //avoid using row by row update, instead we use batch update to protect the integrity of data
        DB::beginTransaction();
        try {
            foreach ($products as $p) {
                $product_schedule = new ProductSchedule();
                $product_schedule->product_id = intval($p);
                $product_schedule->schedule_id = intval($schedule_id);
                $product_schedule->save();
                array_push($result, $product_schedule);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error($e->getMessage(), 400);
        }

        return response()->success($result, 200);
    }

    /**
     * get product information by schedule id".
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProductBySchedule($schedule_id)
    {
        //validate
        if (!is_numeric($schedule_id) || $schedule_id <= 0) {
            return response()->error('The given data was invalid.', 400);
        }

        $user_id = Auth::id();
        $schedule = User::find($user_id)
            ->shop->schedules
            ->where('id', '=', $schedule_id)
            ->first();

        //this user dont have permission to this schedule id
        if (empty($schedule)) {
            return response()->error('Access deny', 401);
        }

        //lấy ra danh sách sản phẩm cùng với check sản phẩm đang active trên schedule hay không
        $products = Product::selectRaw("distinct products.*, products_schedules.schedule_id,
        (CASE WHEN products_schedules.schedule_id= " . $schedule_id . " THEN true
        ELSE false
        END )AS scheduled_activation")
            ->join('shops', 'shops.id', '=', 'products.shop_id')
            ->leftJoin('products_schedules', 'products.id', '=', 'product_id')
            ->orderBy('scheduled_activation', 'desc')
            ->with(['images', 'categories', 'unit', 'warehouse'])
            ->get();

        return response()->success($products, 200);
    }

    /**
     * get active product from schedule id and shop information for "ghé shop page".
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getActiveProductOnSchedule($schedule_id)
    {
        if (!is_numeric($schedule_id) || $schedule_id <= 0) {
            return response()->error('The given data was invalid.', 400);
        }

        $schedule = Schedule::where('id', '=', $schedule_id)->first();

        if (empty($schedule)) {
            return response()->error("SCHEDULE NOT FOUND", 404);
        }

        $products = Product::join('products_schedules', 'products_schedules.product_id', '=', 'products.id')
            ->select(
                'products.id',
                'shop_id',
                'products.name',
                'products.description',
                'cost_per_unit',
                'schedule_id'
            )
            ->with(['images', 'categories', 'unit', 'warehouse'])
            ->where('schedule_id', '=', $schedule_id)
            ->paginate(12);

        $shop = Shop::where('shops.id', '=', $schedule->shop_id)->with('image')->get();

        if (!$products->count()) {
            return response()->error("PRODUCT NOT FOUND", 404);
        }

        return response()->json([
            "success" => true,
            "data" => $products,
            "shop" => $shop
        ], 200);
    }
}
