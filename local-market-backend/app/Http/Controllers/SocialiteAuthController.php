<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

class SocialiteAuthController extends Controller
{
    use HasApiTokens;
    //
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
        return response()->json([
            'url' => $url
        ]);
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider, RegisterController $registerController)
    {
        $user  = Socialite::driver($provider)->stateless()->user();

        if (!$user->token) {
            dd('failed');
        }
        $appUser = User::whereEmail($user->email)->first();
        if (!$appUser) {
            //create user and add user
            $appUser = $registerController->run([
                'name' => $user->name,
                'password' => Str::random(7),
                'email' => $user->email
            ]);
            $socialAccounts = SocialAccount::create([
                'user_id' => $appUser->id,
                'provider_user_id' => $user->id,
                'provider' => $provider,
            ]);
        } else {
            // already this user
            $socialAccounts = $appUser->socialAccounts()->where('provider', $provider)->first();
            if (!$socialAccounts) {
                // create social Accounts
                $socialAccounts = SocialAccount::create([
                    'provider' => $provider,
                    'provider_user_id' => $user->id,
                    'user_id' => $appUser->id
                ]);
            }
        }

        $passportToken = $appUser->createToken('Login token')->accessToken;
        return response()->json([
            'access_token' => $passportToken
        ]);
        // $user->token;
    }
}
