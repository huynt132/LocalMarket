<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function me()
    {
        return Auth::user();
    }
    public function changeDetails(Request $request)
    {
        $user = User::findOrFail(Auth::id());
        $user->name = $request->name;
        $user->email = $request->email;
        $user->age = $request->age;
        $user->phone_number = $request->phone_number;
        $user->address = $request->address;
        if ($user->save()) {
            return response()->json([
                "success" => true,
            ]);
        }
        return response()->json(["success" => false]);
    }

    //sua thanh base64
    public function uploadImage(Request $request)
    {
        $user = User::findOrFail(Auth::id());

        $imageFileType = strtolower(pathinfo($request->file('file'), PATHINFO_EXTENSION));
        // Convert to base64
        $image_base64 = base64_encode(file_get_contents($request->file('file')));
        $image_content = 'data:image/' . $imageFileType . ';base64,' . $image_base64;
        
        $user->avatar = $image_content;
        if ($user->save()) {
        return response()->json([
        "success" => true,
        ]);
        }
        return response()->json(["success" => false]);
    }
}
