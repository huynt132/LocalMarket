<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'products';
    //defind primary key when ::find

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_id',
        'name',
        'description',
        'cost_per_unit',
        'unit_id'
    ];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function warehouse()
    {
        return $this->hasOne(Warehouse::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_products', 'product_id', 'category_id')->withTimestamps();
    }

    public function cartItems()
    {
        return $this->hasMany(Category::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function schedules()
    {
        return $this->belongsToMany(Schedule::class, 'products_schedules', 'product_id', 'schedule_id')->withTimestamps();
    }
}
