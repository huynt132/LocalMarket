<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name'];
    protected $hidden = ['pivot'];
    use HasFactory;
    public function products()
    {
        return $this->belongsToMany(Product::class)->withTimestamps();
    }
}
