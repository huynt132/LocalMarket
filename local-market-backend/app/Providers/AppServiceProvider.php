<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($data, $status_code) {
            return response()->json([
                'success' => true,
                'data' => $data,
            ], $status_code);
        });

        Response::macro('error', function ($error, $status_code) {
            return response()->json([
                'success' => false,
                'message' => $error,
            ], $status_code);
        });

        //prevent lazy load sẽ báo khi meet n+1 problems
        Model::preventLazyLoading(!app()->isProduction());
    }
}
