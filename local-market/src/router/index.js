import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
// import About from '../views/NguoiBan.vue'
import ManagerShop from "../views/admin/ManagerShop.vue";
import Contact from "../views/user/Contact.vue";
import productList from "../views/user/ProductList";
import NewList from "../views/user/NewList.vue";
import RevenueManager from "../views/admin/revenueStatistics.vue";
import ProductManager from "../views/admin/ManageProduct.vue";
import ShopDetail from "../views/user/Shop_Detail.vue";
import LoginUser from "../views/user/Login.vue";
import ShoppingCart from "../views/user/ShoppingCart.vue";
import ProductDetail from "../views/user/Product_Detail.vue";
import PurchaseHistory from "../views/user/PurchaseHistory.vue";
import ListProd from "../views/user/ListProd.vue";


import UserProfile from "../views/user/User.vue";

import Middlewares from "../middlewares/";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/nguoiban/:id",
    name: "Nguoiban",
    component: () => import("@/views/NguoiBan.vue"),
    meta: {
      middleware: [Middlewares.auth],
    },
  },
  {
    path: "/profile",
    name: "UserProfile",
    component: UserProfile,
    meta: {
      middleware: [Middlewares.auth],
    },
  },
  {
    path: "/login/facebook/callback",
    name: "Login",
    component: () => import("@/views/user/LoginFacebook.vue"),
  },
  {
    path: "/managershop/:id",
    name: "ManagerShop",
    component: ManagerShop,
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact,
  },
  {
    path: "/productList",
    name: "productList",
    component: productList,
  },
  {
    path: "/newlist",
    name: "NewList",
    component: NewList,
  },
  {
    path: "/revenueManager/:id",
    name: "RevenueManager",
    component: RevenueManager,
  },
  {
    path: "/ProductManager/:id",
    name: "ProductManager",
    component: ProductManager,
  },
  {
    path: "/shopDetail",
    name: "ShopDetail",
    component: ShopDetail,
  },
  {
    path: "/login",
    name: "LoginUser",
    component: LoginUser,
  },
  {
    path: "/productdetail",
    name: ProductDetail,
    component: ProductDetail,
  },
  {
    path: "/purchHistory",
    name: "PurchaseHistory",
    component: PurchaseHistory,
  },
  {
    path: "/shoppingCart",
    name: "ShoppingCart",
    component: ShoppingCart,
  },
  {
    path: "/listProd",
    name: ListProd,
    component: ListProd,
  },
];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

function nextCheck(context, middleware, index) {
  const nextMiddleware = middleware[index];

  if (!nextMiddleware) return context.next;

  return (...parameters) => {
    context.next(...parameters);
    const nextMidd = nextCheck(context, middleware, index + 1);

    nextMiddleware({ ...context, next: nextMidd });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];

    const ctx = {
      from,
      next,
      router,
      to,
    };

    const nextMiddleware = nextCheck(ctx, middleware, 1);

    return middleware[0]({ ...ctx, next: nextMiddleware });
  }

  return next();
});

export default router;
