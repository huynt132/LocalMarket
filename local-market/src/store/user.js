import axios from "../axios/index.js";

const state = {
  isLoggedIn: false,
  userDetails: {},
};

const getters = {
  loggedIn(state) {
    return state.isLoggedIn;
  },
  userDetails(state) {
    return state.userDetails;
  },
};

const mutations = {
  setLoggedIn(state, payload) {
    state.isLoggedIn = payload;
  },
  setUserDetails(state, payload) {
    state.userDetails = payload;
  },
};

const actions = {
  loginGithub() {
    return new Promise((resolve, reject) => {
      axios
        .get("api/login/facebook/redirect")
        .then((resp) => {
          resolve(resp);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  loginGithubCallback(ctx, payload) {
    return new Promise((resolve, reject) => {
      axios
        .get("login/facebook/callback", {
          params: payload,
        })
        .then((response) => {
          if (response.data.access_token) {
            localStorage.setItem("token", response.data.access_token);
            ctx.commit("setLoggedIn", true);
            ctx.dispatch("me").then(() => resolve(response));
            resolve(response);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  setLoggedInState(ctx) {
    return new Promise((resolve) => {
      if (localStorage.getItem("token")) {
        ctx.commit("setLoggedIn", true);
        resolve(true);
      } else {
        ctx.commit("setLoggedIn", false);
        resolve(false);
      }
    });
  },
  logoutUser(ctx) {
    return new Promise((resolve) => {
      localStorage.removeItem("token");
      ctx.commit("setLoggedIn", false);
      resolve(true);
    });
  },
  me(ctx) {
    return new Promise((resolve, reject) => {
      axios
        .get("me")
        .then((response) => {
          ctx.commit("setUserDetails", response.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  updateDetails(ctx, payload) {
    return new Promise((resolve, reject) => {
      axios
        .post("change-details",payload)
        .then((response) => {
          if (response.data.success) {
            resolve(response);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  updateUpload(ctx, payload) {
    return new Promise((resolve, reject) => {
      axios
        .post("upload", payload)
        .then((response) => {
          if (response.data.success) {
            resolve(response);
          } else {
            reject(response);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  addShop(ctx,payload){
    return new Promise((resolve,reject)=>{
        axios
        .post('addShop/'+state.userDetails.id,payload)
        .then((response)=>{
            if(response.data.success){
               
                resolve(response)
            }else{
                reject(response)
            }
        })
    })
},
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
