
import axios from "../axios/index.js";

const state={
    isAddShop:false,
    shopDetails: {}
};

const getters={
    shopDetails(state){
        return state.shopDetails;
    },
    isAddShop(state){
        return state.isAddShop;
    }
};

const mutations={
    setShopDetails(state, payload){
        state.shopDetails = payload;
    },
    setAddShop(state,payload){
        state.addShop = payload;
    }
};

const actions={
    infoShop(ctx) {
        return new Promise((resolve, reject) => {
          axios
            .get('shops')
            .then((response) => {
              ctx.commit('setShopDetails',response.data);
              resolve(response);
            })
            .catch((error) => {
              reject(error);
            });
        });
      },
    updateShop(ctx,payload){
          return new Promise((resolve,reject)=>{
              axios
              .put('shops/'+state.shopDetails.shop_id,{
                shop_name:payload.shop_name,
                shop_description:payload.shop_description
              })
              .then((response)=>{
                ctx.commit('setShopDetails',response.data);
                  resolve(response)
              })
              .catch((error)=>{
                  reject(error)
              })
          })
    },
    addShop(ctx,payload){
        return new Promise((resolve,reject)=>{
            axios
            .post('shops',payload)
            .then((response)=>{
                if(response.data.success){
                   
                    resolve(response)
                }else{
                    reject(response)
                }
            })
        })
      },
}

export default{
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
}